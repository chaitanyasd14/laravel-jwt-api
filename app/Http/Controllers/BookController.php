<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Book;

class BookController extends Controller
{
    protected $user;


    public function __construct()
    {
        $this->middleware('auth:api');
        $this->user = $this->guard()->user();

    }

    public function index()
    {
        return Book::get()->toJson();
    }

    public function BookCrud($request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name'     => 'required|string',
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'status' => false,
                    'errors' => $validator->errors(),
                ],
                400
            );
        }

        if($request->status == 'add')
        {
            if($book = Book::updateOrCreate(['id'=>$request->id],$request->name))
            {
                return response()->json(
                    [
                        'status' => true,
                        'todo'   => $book,
                    ]
                );
            }
            else {
                return response()->json(
                    [
                        'status'  => false,
                        'message' => 'Oops, the todo could not be saved.',
                    ]
                );
            }
        }elseif($request->status == 'delete')
        {
            Book::where('id',$request->id)->delete();
            return response()->json([
                'status' => true,
                'message' => 'books deleted..!'
            ]);
        }
        else{
            return response()->json(
                    [
                        'status'  => false,
                        'message' => 'Oops, try later',
                    ]
                );
        }
    }
}
