<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Master_gender extends Model
{
    use HasFactory;

    protected $table = 'master_genders';

    protected $fillable = ['name'];



    public function Master_gender()
    {
        return $this->hasMany('App\Model\User');
    }
}
