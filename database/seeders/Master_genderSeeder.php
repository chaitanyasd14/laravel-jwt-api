<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Master_gender;

class Master_genderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gender = [
            ['name' => 'male'],['name'=>'female'],['name'=>'other']
        ];

        foreach($gender as $genders)
        {
            Master_gender::create($genders);
        }
    }
}
